import React from "react";
import styled from "styled-components";

const Equal = styled.button`
@media screen and (max-width: 500px) {
 width:20%;
    }
width 28%;
height 80%;
background-
color: #FFC200;
text-color:white;
border-radius:10px;
margin 5px;
font-size:0.8rem;

border: 1px solid white;
box-shadow: 0px 0px 3px grey;
`;

var error;

function Equals(props) {
  function clicked() {
    try {
      var equation = document.getElementById("displayScreen").innerHTML;

      if (equation.includes("--")) {
        while (equation.includes("--")) {
          equation = equation.replace("--", "+");
        }
      } else if (equation.includes("++")) {
        equation = equation.replace("++", "+");
      } else if (equation.includes("//")) {
        equation = equation.replace("//", "/");
      } else if (equation.includes("**")) {
        equation = equation.replace("**", "*");
      } else if (equation.includes("-*")) {
        error = "sorry invalid operation (-*) please try again ";
      } else if (equation.includes("-/")) {
        error = "sorry invalid operation (-/) please try again ";
      } else if (equation.includes("..")) {
        error =
          "sorry invalid operation equation includes (..) please try again ";
      }

      var result = eval(equation);

      document.getElementById("outputDisplay").innerHTML = `${result}`;
      document.getElementById("displayScreen").innerHTML = "";
    } catch (err) {
      alert(` ${error} `);
    }
  }
  return <Equal onClick={clicked}>{props.value}</Equal>;
}

export default Equals;
