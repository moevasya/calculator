import React, { createContext } from "react";
import styled from "styled-components";
import Button from "./Button";
import Operation from "./Operations";
import Equals from "./EqualButton";
import CC from "./CC";

const SubCont = styled.section`

width 100%;
display:block;
height:20%;
justify-content: center;


`;
const BContainer = styled.section`
@media screen and (max-width: 500px) {
padding-left:7.5%;
}
position: absolute;

height 65%;
width 90%;
margin: auto;
top: 28%;
left:5%;
padding :9%;
padding-top:0%;
padding-bottom0px;




`;

function ButtonContainer() {
  return (
    <BContainer>
      <SubCont>
        <Button value={7} />
        <Button value={8} />
        <Button value={9} />
        <Operation value={"+"} />
      </SubCont>
      <SubCont>
        <Button value={4} />
        <Button value={5} />
        <Button value={6} />
        <Operation value={"-"} />
      </SubCont>
      <SubCont>
        <Button value={1} />
        <Button value={2} />
        <Button value={3} />
        <Operation value={"*"} />
      </SubCont>
      <SubCont>
        <Button value={0} />
        <Button value={"00"} />

        <Button value={"."} />
        <Operation value={"/"} />
      </SubCont>
      <SubCont>
        <Equals value={"="} />
        <CC value={"CE"} />
        <CC value={"C"} />
      </SubCont>
    </BContainer>
  );
}

export default ButtonContainer;
