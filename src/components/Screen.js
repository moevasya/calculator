import React from "react";
import styled from "styled-components";

const Display = styled.output`
  height: 50%;
  width: 100%;
  margin: 5px;
  position: absolute;
  display: block;
  top: 5%;
  left: 5%;
`;

const Output = styled.output`
  height: 50%;
  width: 100%;
  color: #43464b;
  padding: 10px;
  position: absolute;
  top: 40%;
  left: 5%;
`;
const ScreenCon = styled.header`
  width: 90%;
  height: 20%;
  background-color: white;
  margin: auto;
  position: absolute;
  top: 5%;
  left: 5%;
  border: 1px solid #a9a9a9;
  border-radius: 20px;
  box-shadow: 0 0 3px #000000;
`;

function Screen() {
  return (
    <ScreenCon>
      <Display id="displayScreen"></Display>
      <Output id="outputDisplay"></Output>
    </ScreenCon>
  );
}
export default Screen;
