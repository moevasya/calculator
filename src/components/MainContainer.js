import React from "react";
import styled from "styled-components";
import Screen from "./Screen";
import ButtonsContainer from "./ButtonsContainer";

const CalcBody = styled.section`

background-color: #FF5E13;
border-radius: 20px;
border:  1px solid #ffffff;
margin: auto;
position: absolute;
height 80%;
width 30%;
left: 35%;
top: 12.5%;
box-shadow: 0 0 5px #000;
z-index:100;
@media screen and (max-width: 500px) {
    width: 70%;
    height: 70%;
    left: 15%;
    top :12.25
    }
`;

function MainContainer() {
  return (
    <CalcBody>
      <Screen />

      <ButtonsContainer />
    </CalcBody>
  );
}

export default MainContainer;
