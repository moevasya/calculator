import React from "react";
import styled from "styled-components";
import MainContainer from "./components/MainContainer";
import "./App.css";
import Background from "./components/BackGround";

const MainWrapper = styled.section`
width:100%
max-width: 100%;
margin: 0 auto;
z-index:100;
`;

const App = () => (
  <>
    <Background />
    <MainContainer />
  </>
);

export default App;
